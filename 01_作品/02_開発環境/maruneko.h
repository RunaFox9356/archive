//============================
//
//  エネミー設定（まぬるねこ）
// Author:hamada ryuuga
//
//============================
#ifndef _MARUNEKO_H_
#define _MARUNEKO_H_

#include "main.h"
#include "motion.h"
#include "enemy.h"
#include "renderer.h"
#include "object3d.h"

class  CMotion;

class CMaru : public CEnemy
{

public:
	CMaru();
	~CMaru();

	HRESULT Init()override;	// 初期化
	void Uninit()override;	// 破棄
	void Update()override;	// 更新
	void Draw()override;	// 描画
	static CMaru *CMaru::Create();


private:
	bool backOn;


};
#endif

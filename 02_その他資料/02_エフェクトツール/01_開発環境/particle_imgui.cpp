//**************************************************
//
// Hackathon ( imgui_property.cpp )
// Author  : katsuki mizuki
// Author  : Tanaka Kouta
// Author  : Hamada Ryuga
// Author  : Yuda Kaito
//
//**************************************************

//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "particle_imgui.h"
//------------------------------
// imgui
//------------------------------
#define IMGUI_DEFINE_MATH_OPERATORS
#include "imgui_internal.h"
#include <implot.h>

//------------------------------
// json
//------------------------------
#include <fstream>
#include "file.h"

//------------------------------
// CPU
//------------------------------
#include <nvml.h>
#pragma comment(lib, "nvml.lib")

//------------------------------
// particle
//------------------------------
#include "main.h"
#include "application.h"
#include "texture.h"
#include "particle_manager.h"
#include "particle_edit.h"

//-----------------------------------------------------------------------------
// 静的変数
//-----------------------------------------------------------------------------
static nvmlDevice_t device;
static nlohmann::json Sin;//リストの生成

// init data so editor knows to take it from here
//unsigned MemoryUsageMegaBytes(void)
//{
//	MEMORYSTATUSEX m = { sizeof m };
//	GlobalMemoryStatusEx(&m);
//	return (unsigned)(((512 * 1024) + (m.ullTotalVirtual - m.ullAvailVirtual)) / (1024 * 1024));
//}

// [src] https://github.com/ocornut/imgui/issues/123
// [src] https://github.com/ocornut/imgui/issues/55

// v1.22 - flip button; cosmetic fixes
// v1.21 - oops :)
// v1.20 - add iq's interpolation code
// v1.10 - easing and colors        
// v1.00 - jari komppa's original

/* To use, add this prototype somewhere..

namespace ImGui
{
int Curve(const char *label, const ImVec2& size, int maxpoints, ImVec2 *points);
float CurveValue(float p, int maxpoints, const ImVec2 *points);
float CurveValueSmooth(float p, int maxpoints, const ImVec2 *points);
};

*/
/*
Example of use:

ImVec2 foo[10];
...
foo[0].x = -1; // init data so editor knows to take it from here
...
if (ImGui::Curve("Das editor", ImVec2(600, 200), 10, foo))
{
// curve changed
}
...
float value_you_care_about = ImGui::CurveValue(0.7f, 10, foo); // calculate value at position 0.7
*/

namespace tween {
	enum TYPE
	{
		LINEAR,

		QUADIN,			// t^2
		QUADOUT,
		QUADINOUT,
		CUBICIN,		// t^3
		CUBICOUT,
		CUBICINOUT,
		QUARTIN,		// t^4
		QUARTOUT,
		QUARTINOUT,
		QUINTIN,		// t^5
		QUINTOUT,
		QUINTINOUT,
		SINEIN,			// sin(t)
		SINEOUT,
		SINEINOUT,
		EXPOIN,			// 2^t
		EXPOOUT,
		EXPOINOUT,
		CIRCIN,			// sqrt(1-t^2)
		CIRCOUT,
		CIRCINOUT,
		ELASTICIN,		// exponentially decaying sine wave
		ELASTICOUT,
		ELASTICINOUT,
		BACKIN,			// overshooting cubic easing: (s+1)*t^3 - s*t^2
		BACKOUT,
		BACKINOUT,
		BOUNCEIN,		// exponentially decaying parabolic bounce
		BOUNCEOUT,
		BOUNCEINOUT,

		SINESQUARE,		// gapjumper's
		EXPONENTIAL,	// gapjumper's
		SCHUBRING1,		// terry schubring's formula 1
		SCHUBRING2,		// terry schubring's formula 2
		SCHUBRING3,		// terry schubring's formula 3

		SINPI2,			// tomas cepeda's
		SWING,			// tomas cepeda's & lquery's
	};

	// }

	// implementation

	static inline
		double ease(int easetype, double t)
	{
		using namespace std;

		const double d = 1.0f;
		const double pi = 3.1415926535897932384626433832795;
		const double pi2 = 3.1415926535897932384626433832795 / 2;

		double p = t / d;

		switch (easetype)
		{
			// Modeled after the line y = x
		default:
		case TYPE::LINEAR: {
			return p;
		}

		// Modeled after the parabola y = x^2
		case TYPE::QUADIN: {
			return p * p;
		}

		// Modeled after the parabola y = -x^2 + 2x
		case TYPE::QUADOUT: {
			return -(p * (p - 2));
		}

		// Modeled after the piecewise quadratic
		// y = (1/2)((2x)^2)             ; [0, 0.5)
		// y = -(1/2)((2x-1)*(2x-3) - 1) ; [0.5, 1]
		case TYPE::QUADINOUT: {
			if (p < 0.5) {
				return 2 * p * p;
			}
			else {
				return (-2 * p * p) + (4 * p) - 1;
			}
		}

		 // Modeled after the cubic y = x^3
		case TYPE::CUBICIN: {
			return p * p * p;
		}

		// Modeled after the cubic y = (x - 1)^3 + 1
		case TYPE::CUBICOUT: {
			double f = (p - 1);
			return f * f * f + 1;
		}

		// Modeled after the piecewise cubic
		// y = (1/2)((2x)^3)       ; [0, 0.5)
		// y = (1/2)((2x-2)^3 + 2) ; [0.5, 1]
		case TYPE::CUBICINOUT: {
			if (p < 0.5) {
				return 4 * p * p * p;
			}
			else {
				double f = ((2 * p) - 2);
				return 0.5 * f * f * f + 1;
			}
		}

		// Modeled after the quartic x^4
		case TYPE::QUARTIN: {
			return p * p * p * p;
		}

		// Modeled after the quartic y = 1 - (x - 1)^4
		case TYPE::QUARTOUT: {
			double f = (p - 1);
			return f * f * f * (1 - p) + 1;
		}

		// Modeled after the piecewise quartic
		// y = (1/2)((2x)^4)        ; [0, 0.5)
		// y = -(1/2)((2x-2)^4 - 2) ; [0.5, 1]
		case TYPE::QUARTINOUT: {
			if (p < 0.5) {
				return 8 * p * p * p * p;
			}
			else {
				double f = (p - 1);
				return -8 * f * f * f * f + 1;
			}
		}

		// Modeled after the quintic y = x^5
		case TYPE::QUINTIN: {
			return p * p * p * p * p;
		}

		// Modeled after the quintic y = (x - 1)^5 + 1
		case TYPE::QUINTOUT: {
			double f = (p - 1);
			return f * f * f * f * f + 1;
		}

		// Modeled after the piecewise quintic
		// y = (1/2)((2x)^5)       ; [0, 0.5)
		// y = (1/2)((2x-2)^5 + 2) ; [0.5, 1]
		case TYPE::QUINTINOUT: {
			if (p < 0.5) {
				return 16 * p * p * p * p * p;
			}
			else {
				double f = ((2 * p) - 2);
				return  0.5 * f * f * f * f * f + 1;
			}
		}

		// Modeled after quarter-cycle of sine wave
		case TYPE::SINEIN: {
			return sin((p - 1) * pi2) + 1;
		}

		// Modeled after quarter-cycle of sine wave (different phase)
		case TYPE::SINEOUT: {
			return sin(p * pi2);
		}

		// Modeled after half sine wave
		case TYPE::SINEINOUT: {
			return 0.5 * (1 - cos(p * pi));
		}

		// Modeled after shifted quadrant IV of unit circle
		case TYPE::CIRCIN: {
			return 1 - sqrt(1 - (p * p));
		}

		// Modeled after shifted quadrant II of unit circle
		case TYPE::CIRCOUT: {
			return sqrt((2 - p) * p);
		}

		// Modeled after the piecewise circular function
		// y = (1/2)(1 - sqrt(1 - 4x^2))           ; [0, 0.5)
		// y = (1/2)(sqrt(-(2x - 3)*(2x - 1)) + 1) ; [0.5, 1]
		case TYPE::CIRCINOUT: {
			if (p < 0.5) {
				return 0.5 * (1 - sqrt(1 - 4 * (p * p)));
			}
			else {
				return 0.5 * (sqrt(-((2 * p) - 3) * ((2 * p) - 1)) + 1);
			}
		}

		// Modeled after the exponential function y = 2^(10(x - 1))
		case TYPE::EXPOIN: {
			return (p == 0.0) ? p : pow(2, 10 * (p - 1));
		}

		// Modeled after the exponential function y = -2^(-10x) + 1
		case TYPE::EXPOOUT: {
			return (p == 1.0) ? p : 1 - pow(2, -10 * p);
		}

		// Modeled after the piecewise exponential
		// y = (1/2)2^(10(2x - 1))         ; [0,0.5)
		// y = -(1/2)*2^(-10(2x - 1))) + 1 ; [0.5,1]
		case TYPE::EXPOINOUT: {
			if (p == 0.0 || p == 1.0) return p;

			if (p < 0.5) {
				return 0.5 * pow(2, (20 * p) - 10);
			}
			else {
				return -0.5 * pow(2, (-20 * p) + 10) + 1;
			}
		}

		// Modeled after the damped sine wave y = sin(13pi/2*x)*pow(2, 10 * (x - 1))
		case TYPE::ELASTICIN: {
			return sin(13 * pi2 * p) * pow(2, 10 * (p - 1));
		}

		// Modeled after the damped sine wave y = sin(-13pi/2*(x + 1))*pow(2, -10x) + 1
		case TYPE::ELASTICOUT: {
			return sin(-13 * pi2 * (p + 1)) * pow(2, -10 * p) + 1;
		}

		// Modeled after the piecewise exponentially-damped sine wave:
		// y = (1/2)*sin(13pi/2*(2*x))*pow(2, 10 * ((2*x) - 1))      ; [0,0.5)
		// y = (1/2)*(sin(-13pi/2*((2x-1)+1))*pow(2,-10(2*x-1)) + 2) ; [0.5, 1]
		case TYPE::ELASTICINOUT: {
			if (p < 0.5) {
				return 0.5 * sin(13 * pi2 * (2 * p)) * pow(2, 10 * ((2 * p) - 1));
			}
			else {
				return 0.5 * (sin(-13 * pi2 * ((2 * p - 1) + 1)) * pow(2, -10 * (2 * p - 1)) + 2);
			}
		}

		// Modeled (originally) after the overshooting cubic y = x^3-x*sin(x*pi)
		case TYPE::BACKIN: { /*
							 return p * p * p - p * sin(p * pi); */
			double s = 1.70158f;
			return p * p * ((s + 1) * p - s);
		}

		// Modeled (originally) after overshooting cubic y = 1-((1-x)^3-(1-x)*sin((1-x)*pi))
		case TYPE::BACKOUT: {
			/*
			double f = (1 - p);
			return 1 - (f * f * f - f * sin(f * pi));
			*/
			double s = 1.70158f;
			return --p, 1.f * (p*p*((s + 1)*p + s) + 1);
		}

		// Modeled (originally) after the piecewise overshooting cubic function:
		// y = (1/2)*((2x)^3-(2x)*sin(2*x*pi))           ; [0, 0.5)
		// y = (1/2)*(1-((1-x)^3-(1-x)*sin((1-x)*pi))+1) ; [0.5, 1]
		case TYPE::BACKINOUT: {
			/*
			if(p < 0.5)
			{
				double f = 2 * p;
				return 0.5 * (f * f * f - f * sin(f * pi));
			}
			else
			{
				double f = (1 - (2*p - 1));
				return 0.5 * (1 - (f * f * f - f * sin(f * pi))) + 0.5;
			}
			*/
			double s = 1.70158f * 1.525f;
			if (p < 0.5) {
				return p *= 2, 0.5 * p * p * (p*s + p - s);
			}
			else {
				return p = p * 2 - 2, 0.5 * (2 + p*p*(p*s + p + s));
			}
		}

#define	tween$bounceout(p) ( \
                (p) < 4/11.0 ? (121 * (p) * (p))/16.0 : \
                (p) < 8/11.0 ? (363/40.0 * (p) * (p)) - (99/10.0 * (p)) + 17/5.0 : \
                (p) < 9/10.0 ? (4356/361.0 * (p) * (p)) - (35442/1805.0 * (p)) + 16061/1805.0 \
                           : (54/5.0 * (p) * (p)) - (513/25.0 * (p)) + 268/25.0 )

		case TYPE::BOUNCEIN: {
			return 1 - tween$bounceout(1 - p);
		}

		case TYPE::BOUNCEOUT: {
			return tween$bounceout(p);
		}

		case TYPE::BOUNCEINOUT: {
			if (p < 0.5) {
				return 0.5 * (1 - tween$bounceout(1 - p * 2));
			}
			else {
				return 0.5 * tween$bounceout((p * 2 - 1)) + 0.5;
			}
		}

#undef tween$bounceout

		case TYPE::SINESQUARE: {
			double A = sin((p)*pi2);
			return A*A;
		}

		case TYPE::EXPONENTIAL: {
			return 1 / (1 + exp(6 - 12 * (p)));
		}

		case TYPE::SCHUBRING1: {
			return 2 * (p + (0.5f - p)*abs(0.5f - p)) - 0.5f;
		}

		case TYPE::SCHUBRING2: {
			double p1pass = 2 * (p + (0.5f - p)*abs(0.5f - p)) - 0.5f;
			double p2pass = 2 * (p1pass + (0.5f - p1pass)*abs(0.5f - p1pass)) - 0.5f;
			double pAvg = (p1pass + p2pass) / 2;
			return pAvg;
		}

		case TYPE::SCHUBRING3: {
			double p1pass = 2 * (p + (0.5f - p)*abs(0.5f - p)) - 0.5f;
			double p2pass = 2 * (p1pass + (0.5f - p1pass)*abs(0.5f - p1pass)) - 0.5f;
			return p2pass;
		}

		case TYPE::SWING: {
			return ((-cos(pi * p) * 0.5) + 0.5);
		}

		case TYPE::SINPI2: {
			return sin(p * pi2);
		}
		}
	}
}

namespace ImGui
{
	// [src] http://iquilezles.org/www/articles/minispline/minispline.htm
	// key format (for dim == 1) is (t0,x0,t1,x1 ...)
	// key format (for dim == 2) is (t0,x0,y0,t1,x1,y1 ...)
	// key format (for dim == 3) is (t0,x0,y0,z0,t1,x1,y1,z1 ...)
	void spline(const float *key, int num, int dim, float t, float *v)
	{
		static signed char coefs[16] = {
			-1, 2,-1, 0,
			3,-5, 0, 2,
			-3, 4, 1, 0,
			1,-1, 0, 0 };

		const int size = dim + 1;

		// find key
		int k = 0; while (key[k*size] < t) k++;

		// interpolant
		const float h = (t - key[(k - 1)*size]) / (key[k*size] - key[(k - 1)*size]);

		// init result
		for (int i = 0; i < dim; i++) v[i] = 0.0f;

		// add basis functions
		for (int i = 0; i < 4; i++)
		{
			int kn = k + i - 2; if (kn < 0) kn = 0; else if (kn > (num - 1)) kn = num - 1;

			const signed char *co = coefs + 4 * i;

			const float b = 0.5f*(((co[0] * h + co[1])*h + co[2])*h + co[3]);

			for (int j = 0; j < dim; j++) v[j] += b * key[kn*size + j + 1];
		}
	}

	float CurveValueSmooth(float p, int maxpoints, const ImVec2 *points)
	{
		if (maxpoints < 2 || points == 0)
			return 0;
		if (p < 0) return points[0].y;

		float *input = new float[maxpoints * 2];
		float output[4];

		for (int i = 0; i < maxpoints; ++i) {
			input[i * 2 + 0] = points[i].x;
			input[i * 2 + 1] = points[i].y;
		}

		spline(input, maxpoints, 1, p, output);

		delete[] input;
		return output[0];
	}

	float CurveValue(float p, int maxpoints, const ImVec2 *points)
	{
		if (maxpoints < 2 || points == 0)
			return 0;
		if (p < 0) return points[0].y;

		int left = 0;
		while (left < maxpoints && points[left].x < p && points[left].x != -1) left++;
		if (left) left--;

		if (left == maxpoints - 1)
			return points[maxpoints - 1].y;

		float d = (p - points[left].x) / (points[left + 1].x - points[left].x);

		return points[left].y + (points[left + 1].y - points[left].y) * d;
	}

	int Curve(const char *label, const ImVec2& size, const int maxpoints, ImVec2 *points)
	{
		int modified = 0;
		int i;
		if (maxpoints < 2 || points == 0)
			return 0;

		if (points[0].x < 0)
		{
			points[0].x = 0;
			points[0].y = 0;
			points[1].x = 1;
			points[1].y = 1;
			points[2].x = -1;
		}

		ImGuiWindow* window = GetCurrentWindow();
		//ImGuiState& g = *GImGui;
		ImGuiContext& g = *GImGui;
		const ImGuiStyle& style = g.Style;
		const ImGuiID id = window->GetID(label);
		if (window->SkipItems)
			return 0;

		ImRect bb(window->DC.CursorPos, window->DC.CursorPos + size);
		ItemSize(bb);
		if (!ItemAdd(bb, NULL))
			return 0;

		// const bool hovered = IsHovered(bb, id);
		const bool hovered = ItemHoverable(bb, id);
		int max = 0;
		while (max < maxpoints && points[max].x >= 0) max++;

		int kill = 0;
		do
		{
			if (kill)
			{
				modified = 1;
				for (i = kill + 1; i < max; i++)
				{
					points[i - 1] = points[i];
				}
				max--;
				points[max].x = -1;
				kill = 0;
			}

			for (i = 1; i < max - 1; i++)
			{
				if (abs(points[i].x - points[i - 1].x) < 1 / 128.0f)
				{
					kill = i;
				}
			}
		} while (kill);


		RenderFrame(bb.Min, bb.Max, GetColorU32(ImGuiCol_FrameBg, 1), true, style.FrameRounding);

		float ht = bb.Max.y - bb.Min.y;
		float wd = bb.Max.x - bb.Min.x;

		if (hovered)
		{
			SetHoveredID(id);
			if (g.IO.MouseDown[0])
			{
				modified = 1;
				ImVec2 pos = (g.IO.MousePos - bb.Min) / (bb.Max - bb.Min);
				pos.y = 1 - pos.y;

				int left = 0;
				while (left < max && points[left].x < pos.x) left++;
				if (left) left--;

				ImVec2 p = points[left] - pos;
				float p1d = sqrt(p.x*p.x + p.y*p.y);
				p = points[left + 1] - pos;
				float p2d = sqrt(p.x*p.x + p.y*p.y);
				int sel = -1;
				if (p1d < (1 / 16.0f)) sel = left;
				if (p2d < (1 / 16.0f)) sel = left + 1;

				if (sel != -1)
				{
					points[sel] = pos;
				}
				else
				{
					if (max < maxpoints)
					{
						max++;
						for (i = max; i > left; i--)
						{
							points[i] = points[i - 1];
						}
						points[left + 1] = pos;
					}
					if (max < maxpoints)
						points[max].x = -1;
				}

				// snap first/last to min/max
				if (points[0].x < points[max - 1].x) {
					points[0].x = 0;
					points[max - 1].x = 1;
				}
				else {
					points[0].x = 1;
					points[max - 1].x = 0;
				}
			}
		}

		// bg grid
		window->DrawList->AddLine(
			ImVec2(bb.Min.x, bb.Min.y + ht * 0.5f),
			ImVec2(bb.Max.x, bb.Min.y + ht * 0.5f),
			GetColorU32(ImGuiCol_TextDisabled), 3);

		window->DrawList->AddLine(
			ImVec2(bb.Min.x, bb.Min.y + ht * 0.25f),
			ImVec2(bb.Max.x, bb.Min.y + ht * 0.25f),
			GetColorU32(ImGuiCol_TextDisabled));

		window->DrawList->AddLine(
			ImVec2(bb.Min.x, bb.Min.y + ht * 0.25f * 3),
			ImVec2(bb.Max.x, bb.Min.y + ht * 0.25f * 3),
			GetColorU32(ImGuiCol_TextDisabled));

		for (i = 0; i < 9; i++)
		{
			window->DrawList->AddLine(
				ImVec2(bb.Min.x + (wd * 0.1f) * (i + 1), bb.Min.y),
				ImVec2(bb.Min.x + (wd * 0.1f) * (i + 1), bb.Max.y),
				GetColorU32(ImGuiCol_TextDisabled));
		}

		// smooth curve
		enum { smoothness = 256 }; // the higher the smoother
		for (i = 0; i <= (smoothness - 1); ++i) {
			float px = (i + 0) / float(smoothness);
			float qx = (i + 1) / float(smoothness);
			float py = 1 - CurveValueSmooth(px, maxpoints, points);
			float qy = 1 - CurveValueSmooth(qx, maxpoints, points);
			ImVec2 p(px * (bb.Max.x - bb.Min.x) + bb.Min.x, py * (bb.Max.y - bb.Min.y) + bb.Min.y);
			ImVec2 q(qx * (bb.Max.x - bb.Min.x) + bb.Min.x, qy * (bb.Max.y - bb.Min.y) + bb.Min.y);
			window->DrawList->AddLine(p, q, GetColorU32(ImGuiCol_PlotLines));
		}

		// lines
		for (i = 1; i < max; i++)
		{
			//直線
			ImVec2 a = points[i - 1];
			ImVec2 b = points[i];
			a.y = 1 - a.y;
			b.y = 1 - b.y;
			a = a * (bb.Max - bb.Min) + bb.Min;
			b = b * (bb.Max - bb.Min) + bb.Min;
			window->DrawList->AddLine(a, b, GetColorU32(ImGuiCol_PlotLinesHovered));
		}

		if (hovered)
		{
			//曲線
			// control points
			for (i = 0; i < max; i++)
			{
				ImVec2 p = points[i];
				p.y = 1 - p.y;
				p = p * (bb.Max - bb.Min) + bb.Min;
				ImVec2 a = p - ImVec2(2, 2);
				ImVec2 b = p + ImVec2(2, 2);
				window->DrawList->AddRect(a, b, GetColorU32(ImGuiCol_PlotLinesHovered));
			}
		}

		// buttons; @todo: mirror, smooth, tessellate
		if (ImGui::Button("Flip")) {
			for (i = 0; i < max; ++i) {
				points[i].y = 1 - points[i].y;
			}
		}
		ImGui::SameLine();

		// curve selector
		const char* items[] = {
			"Custom",

			"Linear",
			"Quad in",
			"Quad out",
			"Quad in  out",
			"Cubic in",
			"Cubic out",
			"Cubic in  out",
			"Quart in",
			"Quart out",
			"Quart in  out",
			"Quint in",
			"Quint out",
			"Quint in  out",
			"Sine in",
			"Sine out",
			"Sine in  out",
			"Expo in",
			"Expo out",
			"Expo in  out",
			"Circ in",
			"Circ out",
			"Circ in  out",
			"Elastic in",
			"Elastic out",
			"Elastic in  out",
			"Back in",
			"Back out",
			"Back in  out",
			"Bounce in",
			"Bounce out",
			"Bounce in out",

			"Sine square",
			"Exponential",

			"Schubring1",
			"Schubring2",
			"Schubring3",

			"SinPi2",
			"Swing"
		};
		static int item = 0;
		if (modified) {
			item = 0;
		}
		if (ImGui::Combo("Ease type", &item, items, IM_ARRAYSIZE(items))) {
			max = maxpoints;
			if (item > 0) {
				for (i = 0; i < max; ++i) {
					points[i].x = i / float(max - 1);
					points[i].y = float(tween::ease(item - 1, points[i].x));
				}
			}
		}

		char buf[128];
		const char *str = label;

		if (hovered) {
			ImVec2 pos = (g.IO.MousePos - bb.Min) / (bb.Max - bb.Min);
			pos.y = 1 - pos.y;

			sprintf(buf, "%s (%f,%f)", label, pos.x, pos.y);
			str = buf;
		}

		RenderTextClipped(ImVec2(bb.Min.x, bb.Min.y + style.FramePadding.y), bb.Max, str, NULL, NULL, ImVec2(0.5f, 0.5f));

		return modified;
	}

};

//-----------------------------------------------------------------------------
// コンストラクタ
//-----------------------------------------------------------------------------
CParticleImgui::CParticleImgui() :
	s_bEffectEnable(true),
	gpu_id(0)
{
	memset(foo, 0, sizeof(foo));
}

//-----------------------------------------------------------------------------
// デストラクタ
//-----------------------------------------------------------------------------
CParticleImgui::~CParticleImgui()
{
}

//-----------------------------------------------------------------------------
// 初期化
//-----------------------------------------------------------------------------
HWND CParticleImgui::Init(HWND hWnd, LPDIRECT3DDEVICE9 pDevice)
{
	// 初期化
	HWND outWnd = CImguiProperty::Init(hWnd, pDevice);

	//GPU
	nvmlInit();//初期化
	nvmlDeviceGetHandleByIndex(gpu_id, &device);

	return outWnd;
}

//-----------------------------------------------------------------------------
// 終了
//-----------------------------------------------------------------------------
void CParticleImgui::Uninit(HWND hWnd, WNDCLASSEX wcex)
{
	CImguiProperty::Uninit(hWnd, wcex);
}

//-----------------------------------------------------------------------------
// 更新
//-----------------------------------------------------------------------------
bool CParticleImgui::Update()
{
#ifdef _DEBUG

	CImguiProperty::Update();

	if (!s_window)
	{// ウインドウを使用しない
		return false;
	}

	static int sliderInt = 0;
	if (ImGui::BeginMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			if (ImGui::MenuItem("Save"))
			{
				sliderInt = 2;
			}
			else if (ImGui::MenuItem("Load"))
			{
				sliderInt = 5;
			}

			ImGui::EndMenu();
		}

		ImGui::EndMenuBar();
	}

	//パーティクルのデータ出力＆読み込み
	if (ImGui::Button("JSON_LOAD"))
	{
		LoadJson(L"data/FILE/DataEffectOutput.json");

		// パーティクルのデータ
		CParticleManager* particleManager = CApplication::GetInstance()->GetParticleManager();
		CParticleEdit* particleEdit = CApplication::GetInstance()->GetParticleEdit();

		// 編集するエミッタ―の情報
		CParticleEmitter* emitter = particleEdit->GetEmitter();

		// 読み込んだ末尾に変更する。
		emitter->SetParticle((particleManager->GetBundledData().end() - 1)->particleData);
		emitter->SetEmitter((particleManager->GetBundledData().end() - 1)->emitterData);
	}

	if (ImGui::Button("JSON_SAVE"))
	{
		CApplication::GetInstance()->GetParticleEdit()->SaveEffect();
	}

	//SINカーブのデータ出力＆読み込み
	if (ImGui::Button("SINDATA"))
	{
		const std::string pathToJSON = "data/FILE/Sintest.json";
		std::ifstream ifs(pathToJSON);

		if (ifs)
		{
			//StringToWString(UTF8toSjis(j["name"]));
			//DataSet.unionsname = StringToWString(UTF8toSjis(j["unions"] ["name"]));
			ifs >> Sin;

			for (int nCnt = 0; nCnt < 10; nCnt++)
			{
				std::string name = "Sin";
				std::string Number = std::to_string(nCnt);
				name += Number;

				foo[nCnt] = ImVec2(Sin[name]["X"], Sin[name]["Y"]);
			}
		}
	}

	if (ImGui::Button("SINOUT"))
	{
		for (int nCnt = 0; nCnt < 10; nCnt++)
		{

			std::string name = "Sin";
			std::string Number = std::to_string(nCnt);
			name += Number;
			Sin[name] = { { "X", foo[nCnt].x } ,{ "Y", foo[nCnt].y } };

		}
		auto jobj = Sin.dump();
		std::ofstream writing_file;
		const std::string pathToJSON = "data/FILE/Sintest.json";
		writing_file.open(pathToJSON, std::ios::out);
		writing_file << jobj << std::endl;
		writing_file.close();
	}

	if (ImGui::Curve("Das editor", ImVec2(600, 200), 10, foo))
	{
		float value_you_care_about = ImGui::CurveValue(0.7f, 10, foo); // calculate value at position 0.7
	}

	unsigned int clockMZ = 0;

	//現在の使用率取得
	nvmlDeviceGetClock(device, NVML_CLOCK_MEM, NVML_CLOCK_ID_CURRENT, &clockMZ);

	float clockNau = (float)clockMZ * 0.1f;

	// GPUの表示
	ImGui::Text("GPU  : %.2f%%", clockNau);

	// FPSの表示
	ImGui::Text("FPS  : %.2f", ImGui::GetIO().Framerate);

	ImGui::Separator();	// 区切り線

	//エフェクト関係
	bool existsChange = false;
	if (ImGui::CollapsingHeader("EffectSetting"))
	{
		existsChange = ParticleProperty();
	}

	ImPlot::ShowDemoWindow();
	ImGui::End();
	return existsChange;
#endif // _DEBUG
}

//-----------------------------------------------------------------------------
// パーティクルのImGui
//-----------------------------------------------------------------------------
bool CParticleImgui::ParticleProperty()
{
	bool existsChange = false;	// 変更があるか調べる
	auto changeDetection = [&existsChange](bool function)
	{
		if (existsChange)
		{
			return;
		}
		existsChange = function;
	};

	static bool useEffect = true;
	CParticleEdit* particleEdit = CApplication::GetInstance()->GetParticleEdit();	// エディター
	CParticleManager* particleManager = CApplication::GetInstance()->GetParticleManager();	// マネジャー
	CParticleManager::BundledData* bundled = particleEdit->GetBundledData();	// 初期値
	CParticleEmitter::Info* emitter = &bundled->emitterData;	// 編集するエミッタ―の情報
	CParticle::Info* particle = &bundled->particleData;			// 編集するパーティクルの情報

	// 全体エミッタ―の情報
	std::list<CParticleEmitter*> emitterList = particleManager->GetEmitter();

	std::vector<char*> listbox_items;
	

	ImGui::BeginChild(ImGui::GetID((void*)0), ImVec2(250, 100), ImGuiWindowFlags_NoTitleBar);
	float a;
	for (int i = 0; i < 1 ; ++i) 
	{
		char name[16] = "a";
		sprintf(name, "item %d", i);
		ImGui::SliderFloat(name, &a, 0.0f, 10.0f);
	}
	ImGui::EndChild();
	ImGui::Text("/* EffectEmitter */");
	{
		for (int i = 0; i < emitterList.size(); i++)
		{
			listbox_items.push_back("a");
		}

		static int listbox_item_current = 1;
		ImGui::ListBox("a", &listbox_item_current, listbox_items.data(), (int)emitterList.size(), 5);
	}

	ImGui::Separator();

	if (ImGui::Checkbox("EffectEnable", &useEffect))
	{
		s_bEffectEnable = !s_bEffectEnable;
	}
	ImGui::Separator();

	if (ImGui::Button("Template"))
	{
		// パーティクルをテンプレート状態にする
		ParticleTemplate();
	}

	ImGui::Separator();
	ImGui::Text("/* Texture */");	//ここTEXよみこみ
	if (ImGui::Button("LOAD TEXTURE"))
	{
		char* fileName = GetFileName();
		GetFile(nullptr, fileName, sizeof(fileName), TEXT("C:\\"));

		if (fileName[0] != '\0')
		{
			std::string File = fileName;
			char * Data = GetBuffer();
			HWND hWnd = GetWnd();
			strcpy(Data, File.c_str());

			SetFileName(Data);

			funcFileSave(hWnd);
		}
	}

	{
		// テクスチャ
		CTexture* pTexture = CApplication::GetInstance()->GetTextureClass();
		int& index = particleManager->GetBundledData()[0].particleData.nIdxTex;

		if (ImGui::BeginCombo("Texture", pTexture->GetPath(index, false).c_str(), 0))
		{// コンボボタン
			for (int i = 0; i < pTexture->GetNumAll(); i++)
			{
				const bool is_selected = (index == i);

				if (ImGui::Selectable(pTexture->GetPath(i, false).c_str(), is_selected))
				{// 選ばれた選択肢に変更
					index = i;
				}

				if (is_selected)
				{// 選択肢の項目を開く
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}
	}

	// エミッタ―の位置を調整
	D3DXVECTOR3 Imguipos = particleEdit->GetEmitter()->GetPos();
	ImGui::Separator();
	ImGui::Text(u8"/* エフェクトの座標 */");
	ImGui::SliderFloat("PosX", &Imguipos.x, 0.0f, (float)CApplication::SCREEN_WIDTH);
	ImGui::SliderFloat("PosY", &Imguipos.y, 0.0f, (float)CApplication::SCREEN_HEIGHT);
	ImGui::Separator();

	particleEdit->GetEmitter()->SetPos(Imguipos);

	// 生成範囲の設定
	ImGui::Text(u8"/* 生成範囲の設定 */");
	changeDetection(ImGui::SliderInt("PopNumber", &emitter->popNumber, 0, 20));
	changeDetection(ImGui::SliderFloat("MaxPopPosX", &emitter->maxPopPos.x, 0, (float)CApplication::SCREEN_WIDTH));
	changeDetection(ImGui::SliderFloat("MinPopPosX", &emitter->minPopPos.x, 0, (float)CApplication::SCREEN_WIDTH));
	changeDetection(ImGui::SliderFloat("MaxPopPosY", &emitter->maxPopPos.y, 0, (float)CApplication::SCREEN_HEIGHT));
	changeDetection(ImGui::SliderFloat("MinPopPosY", &emitter->minPopPos.y, 0, (float)CApplication::SCREEN_HEIGHT));
	ImGui::Separator();

	ImGui::Text(u8"/* エフェクトの動き */");
	changeDetection(ImGui::InputFloat2("SettingEffectMove", particle->move, "%f"));
	changeDetection(ImGui::SliderFloat("MoveX", &particle->move.x, -100.0f, 100.0f));
	changeDetection(ImGui::SliderFloat("MoveY", &particle->move.y, -100.0f, 100.0f));

	//詳細
	if (ImGui::CollapsingHeader("Details"))
	{
		//rot計算用
		ImGui::Text("/* Rot */");
		changeDetection(ImGui::InputFloat3("SettingEffectRot", particle->rot, "%f"));
		changeDetection(ImGui::SliderFloat("RotZ", &particle->rot.z,-D3DX_PI, D3DX_PI));

		static bool s_bRot = false;
		if (ImGui::Checkbox("BackRot", &s_bRot))
		{
			particle->bBackrot = !particle->bBackrot;
		}

		ImGui::Separator();
		ImGui::Text(u8"/* エフェクトのサイズ */");
		changeDetection(ImGui::SliderFloat("Scale", &particle->fScale, 0.0f, 100.0f));
		changeDetection(ImGui::SliderFloat("ScaleTransfome.x", &particle->scaleTransition.x, -100.0f, 100.0f));
		changeDetection(ImGui::SliderFloat("ScaleTransfome.y", &particle->scaleTransition.y, -100.0f, 100.0f));
		ImGui::Separator();
		ImGui::Text(u8"/* エフェクトの寿命 */");
		changeDetection(ImGui::SliderInt("Life", (int*)&particle->nLife, 0, 500));
		ImGui::Separator();
		ImGui::Text("/* Radius */");
		changeDetection(ImGui::SliderFloat("Radius", &particle->fRadius, 0.0f, 100.0f));
		ImGui::Separator();
		ImGui::Text("/* Angle */");
		changeDetection(ImGui::SliderAngle("Angle", &particle->fAngle, 0.0f, 2000.0f));
		ImGui::Separator();
		ImGui::Text("/* Attenuation */");
		changeDetection(ImGui::SliderFloat("Attenuation", &particle->fAttenuation, 0.0f, 1.0f));

		//挙動おかしくなっちゃった時用
		if (ImGui::Button("DataRemove"))
		{
			//DeleteParticleAll();
			//RemoveAngle();
		}
	}

	if (ImGui::CollapsingHeader(u8"色の設定"))
	{
		//カラーパレット
		changeDetection(ColorPalette4(u8"色", (float*)&particle->color));

		// ランダムカラー
		ImGui::Checkbox(u8"ランダム幅", &particle->color.bColRandom);

		if (particle->color.bColRandom)
		{
			changeDetection(ColorPalette4(u8"RandamMax", (float*)&particle->color.colRandamMax));
			changeDetection(ColorPalette4(u8"RandamMin", (float*)&particle->color.colRandamMin));
		}

		// カラートラディション
		ImGui::Checkbox(u8"カラートラディション", &particle->color.bColTransition);

		if (particle->color.bColTransition)
		{// 目的の色
			changeDetection(ColorPalette4(u8"目的の色", (float*)&particle->color.destCol));

			ImGui::Checkbox("RandomTransitionTime", &particle->color.bRandomTransitionTime);

			if (!particle->color.bRandomTransitionTime)
			{
				changeDetection(ImGui::SliderInt("EndTime", &particle->color.nEndTime, 0, particle->nLife));
			}
		}
	}

	//グラデーション
	if (ImGui::CollapsingHeader(u8"グラデーション"))
	{
		static float s_fCustR[10];
		static float s_fCustG[10];
		static float s_fCustB[10];
		static int s_nSpeed = 1;
		static int selecttype = 0;

		ImGui::RadioButton("Custom", &selecttype, 1);

		if (selecttype == 1)
		{
			static int s_nSetTime = 0;
			static int nTypeNum = 0;
			const char *Items[] = { "Red", "Green", "Blue" };
			ImGui::Combo("ColorType", &nTypeNum, Items, IM_ARRAYSIZE(Items));

			//赤
			switch (nTypeNum)
			{
			case 0:
				ImGui::PlotLines("Custom Gradation", s_fCustR, IM_ARRAYSIZE(s_fCustR), 0, nullptr, -0.5f, 0.5f, ImVec2(0, 100));

				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.0f, 0.0f, 1.0f));
				ImGui::SliderFloat("Red", &s_fCustR[s_nSetTime], -0.5f, 0.5f);
				ImGui::PopStyleColor();
				break;
			case 1:
				ImGui::PlotLines("Custom Gradation", s_fCustG, IM_ARRAYSIZE(s_fCustG), 0, nullptr, -0.5f, 0.5f, ImVec2(0, 100));

				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 1.0f, 0.0f, 1.0f));
				ImGui::SliderFloat("Green", &s_fCustG[s_nSetTime], -0.5f, 0.5f);
				ImGui::PopStyleColor();
				break;
			case 2:
				ImGui::PlotLines("Custom Gradation", s_fCustB, IM_ARRAYSIZE(s_fCustB), 0, nullptr, -0.5f, 0.5f, ImVec2(0, 100));

				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.6f, 1.0f, 1.0f));
				ImGui::SliderFloat("Blue", &s_fCustB[s_nSetTime], -0.5f, 0.5f);
				ImGui::PopStyleColor();
				break;
			default:
				break;
			}

			ImGui::SliderInt("SetKey", &s_nSetTime, 0, 9);
			ImGui::SliderInt("Speed", &s_nSpeed, 1, 30);		//数値が高くなると変化速度がゆっくりになる

			/*グラフの全ての色の数値を０にする*/
			if (ImGui::Button("All Zero"))
			{
				for (int i = 0; i < 10; i++)
				{
					memset(&s_fCustR[i], 0, sizeof(s_fCustR[i]));
					memset(&s_fCustG[i], 0, sizeof(s_fCustG[i]));
					memset(&s_fCustB[i], 0, sizeof(s_fCustB[i]));
				}
			}
		}

		ImGui::RadioButton("Gradation None", &selecttype, 0);

		static int s_nCounter;
		static int s_nTimer;
		static int s_nColNum;

		switch (selecttype)
		{
		case 1:
			s_nCounter++;

			//ゼロ除算回避
			if (s_nSpeed <= 0)
			{
				s_nSpeed = 1;
			}

			if ((s_nCounter % s_nSpeed) == 0)
			{//一定時間経過
				s_nTimer++;

				if (s_nTimer >= 5)
				{
					particle->color.colTransition = D3DXCOLOR(s_fCustR[s_nColNum], s_fCustG[s_nColNum], s_fCustB[s_nColNum], 0.0f);
					s_nColNum++;
					s_nTimer = 0;
				}
			}

			if (s_nCounter >= 60)
			{
				s_nCounter = 0;
			}

			if (s_nColNum >= 10)
			{
				s_nColNum = 0;
			}

			break;

		case 2:

			break;
		case 0:
			break;
		default:
			break;
		}

		changeDetection(ImGui::SliderFloat("Alpha", &particle->color.colTransition.a, -0.5f, 0.0f));
	}

	// αブレンディングの種類
	if (ImGui::CollapsingHeader(u8"aブレンディングの種類"))
	{
		// 変数宣言
		int	nBlendingType = (int)particle->alphaBlend;		// 種別変更用の変数

		changeDetection(ImGui::RadioButton(u8"加算", &nBlendingType, 0));
		changeDetection(ImGui::RadioButton(u8"減算", &nBlendingType, 1));
		changeDetection(ImGui::RadioButton(u8"なし", &nBlendingType, 2));

		particle->alphaBlend = (CParticle::ALPHABLENDTYPE)nBlendingType;
	}

	return existsChange;
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CParticleImgui::Draw()
{
	CImguiProperty::Draw();
}

//--------------------------------------------------
// エフェクト使用状況の取得
//--------------------------------------------------
bool CParticleImgui::bSetImguiParticle(void)
{
	return false;
}

//--------------------------------------------------
// パーティクルのテンプレート
//--------------------------------------------------
void CParticleImgui::ParticleTemplate(void)
{
	CParticleManager* manager = CApplication::GetInstance()->GetParticleManager();
	CParticleManager::BundledData& templateData = manager->GetBundledData()[0];
	// 編集するエミッタ―の情報
	CParticleEdit* particleEdit = CApplication::GetInstance()->GetParticleEdit();
	CParticleEmitter* emitter = particleEdit->GetEmitter();

	// 位置の取得
	D3DXVECTOR3 Imguipos = emitter->GetPos();
	Imguipos.x = CApplication::SCREEN_WIDTH * 0.5f;
	Imguipos.y = CApplication::SCREEN_HEIGHT * 0.5f;
	emitter->SetPos(Imguipos);

	// パーティクルデータの取得
	CParticle::Info& particleInfo = templateData.particleData;
	CParticleEmitter::Info& emitterInfo = templateData.emitterData;
	emitterInfo.maxPopPos.x = 0.0f;
	emitterInfo.maxPopPos.y = 0.0f;
	emitterInfo.minPopPos.x = 0.0f;
	emitterInfo.minPopPos.y = 0.0f;
	particleInfo.move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	particleInfo.rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	particleInfo.color.colBigin = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);
	particleInfo.color.destCol = D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f);
	particleInfo.color.colRandamMax = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	particleInfo.color.colRandamMin = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);
	particleInfo.color.colTransition = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);
	particleInfo.nLife = 60;
	particleInfo.fScale = 50.0f;
	particleInfo.fRadius = 4.5f;
	particleInfo.fAngle = 20.5f;
	particleInfo.fAttenuation = 0.98f;
	particleInfo.alphaBlend = (CParticle::ALPHABLENDTYPE)0;
}

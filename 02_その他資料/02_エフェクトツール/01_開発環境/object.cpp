//=========================================
// 
// オブジェクトクラス
// Author YudaKaito
// 
//=========================================
#include "object.h"

const int CObject::NUM_MAX;
CObject* CObject::m_object[NUM_MAX] = {};
int CObject::m_numAll = 0;
 
//=============================================================================
// コンストラクタ
//=============================================================================
CObject::CObject() :
	pos(D3DXVECTOR3(0.0f,0.0f,0.0f)),
	createIdx(0)
{
	for (int i = 0; i < NUM_MAX; i++)
	{
		if (m_object[i] == nullptr)
		{
			m_numAll++;
			m_object[i] = this;
			createIdx = i;
			break;
		}
	}
}

//=============================================================================
// デストラクタ
//=============================================================================
CObject::~CObject()
{
}

//============================================================================
// 破棄
//=============================================================================
void CObject::Release()
{
	int idx = createIdx;
	m_object[idx]->Uninit();
	delete m_object[idx];
	m_object[idx] = nullptr;
	m_numAll--;
}

//=============================================================================
// 全ての破棄
//=============================================================================
void CObject::ReleaseAll()
{
	for (int i = 0; i < NUM_MAX; i++)
	{
		if (m_object[i] != nullptr)
		{
			m_object[i]->Uninit();
			delete m_object[i];
			m_object[i] = nullptr;
		}
	}
}

//=============================================================================
// 全ての更新
//=============================================================================
void CObject::UpdateAll()
{
	for (int i = 0; i < NUM_MAX; i++)
	{
		if (m_object[i] != nullptr)
		{
			m_object[i]->Update();
		}
	}
}

//=============================================================================
// 全ての描画
//=============================================================================
void CObject::DrawAll()
{
	for (int i = 0; i < NUM_MAX; i++)
	{
		if (m_object[i] != nullptr)
		{
			m_object[i]->Draw();
		}
	}
}
